package ru.pbt.dmplayer;

import android.os.Environment;

import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.util.ArrayList;

import ru.pbt.dmplayer.filter.AudioFilter;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class AudioListTest {
    private File rootFile;
    private ArrayList<File> list;
    private AudioFilter filter;

    /**
     * Подготовление данных для тестов
     */
    @Before
    public void prepare() {
        String path = Environment.getExternalStorageDirectory().
                getAbsolutePath() + "/dmedia/";
        rootFile = new File(path);
        list = new ArrayList<>();
        filter = new AudioFilter();
        initAudioList(rootFile, filter);
    }

    /**
     * Тест, проверяющий - существует ли папка dmedia
     */
    @Test
    public void existTest() {
        assertTrue(rootFile.exists());
    }

    /**
     * Тест, проверяющий файд - директория ли он
     */
    @Test
    public void folderTest() {
        assertTrue(rootFile.isDirectory());
    }

    /**
     * Тест, проверяющий - заполнен ли список
     * аудио
     */
    @Test
    public void nonEmptyTest() {
        assertFalse(list.isEmpty());
    }

    /**
     * Тест, проверяющий список данных на корректность
     */
    @Test
    public void dataTest() {
        File temp;
        for (File file: list){
            temp = new File(file.getPath());
            if (!filter.accept(temp)){
                fail();
            }
        }
    }

    /**
     * Инициализирует список данных
     * @param rootDir корневая директория
     * @param audioFilter аудиофильтер
     */
    private void initAudioList(File rootDir, AudioFilter audioFilter) {
        File[] temp = rootDir.listFiles();
        if (temp == null) return;
        for (File file : temp) {
            if (file.isDirectory()) initAudioList(file, audioFilter);

            if (audioFilter.accept(file)) {
                list.add(file);
            }
        }
    }

}
