package ru.pbt.dmplayer;

import android.media.MediaPlayer;
import android.os.Environment;

import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.IOException;

import ru.pbt.dmplayer.filter.AudioFilter;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class MediaTest {
    private MediaPlayer player;
    private File dataSrc;

    /**
     * Метод для инициализации необходимых
     * инструментов для тестирования
     */
    @Before
    public void prepare() {
        player = new MediaPlayer();
        getTestData();
    }

    /**
     * Метод для получения тестовых данных из папки dmedia
     */
    private void getTestData() {
        String path = Environment.getExternalStorageDirectory().
                getAbsolutePath() + "/dmedia/";
        File root = new File(path);
        getMediaFile(root, new AudioFilter());
    }

    /**
     * Метод для получения первого найденного аудиофайла из папки dmedia
     *
     * @param root        корневая директория
     * @param audioFilter аудиофильтер
     */
    private void getMediaFile(File root, AudioFilter audioFilter) {
        File[] files = root.listFiles();
        if (files == null) return;
        for (File file : files) {
            if (file.isDirectory()) {
                getMediaFile(file, audioFilter);
            } else if (audioFilter.accept(file)) {
                dataSrc = file;
                return;
            }
        }
    }

    /**
     * Метод для тестировнаия медиаплеера - не играет песня
     */
    @Test
    public void notPlay() {
        assertFalse(player.isPlaying());
    }

    /**
     * Метод для тестировнаия медиаплеера - играет песня
     */
    @Test
    public void testPlay() {
        try {
            player.setDataSource(dataSrc.getAbsolutePath());
            player.prepare();
            player.start();
            assertTrue(player.isPlaying());
        } catch (IOException e) {
            fail();
        }
    }

    /**
     * Метод для тестировнаия медиаплеера - остановлено ли произведение
     * песни
     */
    @Test
    public void stopTest() {
        player.stop();
        assertFalse(player.isPlaying());
    }

    /**
     * Метод для тестировнаия медиаплеера - сменен ли
     * источник данных для медиаплеера
     */
    @Test
    public void setDataSrcTest() {
        player.reset();
        try {
            player.setDataSource(dataSrc.getAbsolutePath());
        } catch (IOException e) {
            fail();
        }
    }
}
