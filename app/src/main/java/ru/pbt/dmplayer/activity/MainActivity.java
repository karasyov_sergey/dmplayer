package ru.pbt.dmplayer.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.io.File;
import java.util.ArrayList;

import ru.pbt.dmplayer.R;
import ru.pbt.dmplayer.audio.AudioFile;
import ru.pbt.dmplayer.filter.AudioFilter;

/**
 * Класс, представляющий собой главную
 * активность приложения
 *
 * @author KSO, Лисова Анастасия 17ИТ17
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String NOT_HAVE_PERMISSION = "Don't have permission";
    public static MainActivity instance;
    private ArrayList<AudioFile> audioList;
    private ArrayList<String> artistList;
    private ArrayList<String> albumList;
    private ArrayList<AudioFile> currentAudioList;
    private MediaPlayer mediaPlayer;
    private int currentPlayed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        currentPlayed = -1;
        instance = this;
        mediaPlayer = new MediaPlayer();
        BottomNavigationView navView = findViewById(R.id.nav_view);
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_track_list, R.id.nav_actor_list, R.id.nav_album_list)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) !=
                        PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1000);
        } else {
            initDataList();
            initWindowPlay();
        }
    }

    /**
     * Инициализирует окно проигрывания
     * приложения
     */
    private void initWindowPlay() {
        ConstraintLayout layout = findViewById(R.id.window_play);
        layout.setOnClickListener(this);
        ImageButton imageButton = findViewById(R.id.btn_play);
        imageButton.setOnClickListener(this);
    }

    /**
     * Инициализирует список необходимых данных
     * относительно загруженных в папку dmedia треков
     */
    private void initDataList() {
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/dmedia/";
        File rootDir = new File(path);
        audioList = new ArrayList<>();
        artistList = new ArrayList<>();
        albumList = new ArrayList<>();
        initAudioList(rootDir, new AudioFilter());
        initArtistList();
        initAlbumList();

        sortTrack(audioList);
        sortStringList(artistList);
        sortStringList(albumList);
    }

    /**
     * Метод для сортировки списка
     * исполнителей и альбомов
     * по алфавиту
     *
     * @param list список
     */
    private void sortStringList(ArrayList<String> list) {
        for (int j = list.size() - 1; j >= 0; j--) {
            for (int i = 0; i < j; i++) {
                if (list.get(i).compareTo(list.get(i + 1)) > 0) {
                    String file = list.get(i);
                    list.set(i, list.get(i + 1));
                    list.set(i + 1, file);
                }
            }
        }
    }

    /**
     * Метод для сортировки списка
     * треков по алфавиту
     *
     * @param list список
     */
    public void sortTrack(ArrayList<AudioFile> list) {
        for (int j = list.size() - 1; j >= 0; j--) {
            for (int i = 0; i < j; i++) {
                if (list.get(i).getAudioName().compareTo(list.get(i + 1).getAudioName()) > 0) {
                    AudioFile file = list.get(i);
                    list.set(i, list.get(i + 1));
                    list.set(i + 1, file);
                }
            }
        }
    }

    /**
     * Инициализирует список альбомов относительно
     * загруженных пользователем в папку dmedia треков
     */
    private void initAlbumList() {
        String album;
        for (int i = 0; i < audioList.size(); i++) {
            album = audioList.get(i).getAlbum();
            if (!albumList.contains(album)) {
                albumList.add(album);
            }
        }
    }

    /**
     * Инициализирует список исполнителей относительно
     * загруженных пользователем в папку dmedia треков
     */
    private void initArtistList() {
        String artist;
        for (int i = 0; i < audioList.size(); i++) {
            artist = audioList.get(i).getArtist();
            if (!artistList.contains(artist)) {
                artistList.add(artist);
            }
        }
    }

    /**
     * Инициализирует список треков относительно
     * загруженных пользователем в папку dmedia треков
     *
     * @param rootDir корневая директория
     * @param audioFilter аудиофильтер
     */
    private void initAudioList(File rootDir, AudioFilter audioFilter) {
        File[] temp = rootDir.listFiles();
        if (temp == null) return;
        for (File file : temp) {
            if (file.isDirectory()) initAudioList(file, audioFilter);

            if (audioFilter.accept(file)) {
                AudioFile audioFile = new AudioFile(file.getAbsolutePath());
                audioList.add(audioFile);
            }
        }
    }

    public static MainActivity getInstance() {
        return instance;
    }

    public ArrayList<AudioFile> getAudioList() {
        return audioList;
    }

    public ArrayList<String> getArtistList() {
        return artistList;
    }

    public ArrayList<String> getAlbumList() {
        return albumList;
    }

    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    public int getCurrentPlayed() {
        return currentPlayed;
    }

    public void setCurrentPlayed(int currentPlayed) {
        this.currentPlayed = currentPlayed;
    }

    public ArrayList<AudioFile> getCurrentAudioList() {
        return currentAudioList;
    }

    public void setCurrentAudioList(ArrayList<AudioFile> currentAudioList) {
        this.currentAudioList = currentAudioList;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == 1000 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            initDataList();
        } else {
            Toast.makeText(this, NOT_HAVE_PERMISSION,
                    Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_play) onBtnPlayClick((ImageButton) v);
        if (v.getId() == R.id.window_play) onWindowPlayClick();
    }

    /**
     * Метод, срабатывающий по нажатии на
     * окно проигрывания
     */
    private void onWindowPlayClick() {
        Intent intent = new Intent(this, WindowPlayActivity.class);
        startActivity(intent);
    }

    /**
     * Метод, срабатывающий по нажатии
     * на кнопку проигрывания
     *
     * @param v вью типа ImageButton
     */
    private void onBtnPlayClick(ImageButton v) {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
            v.setImageResource(R.drawable.play);

        } else if (!mediaPlayer.isPlaying()) {
            mediaPlayer.start();
            v.setImageResource(R.drawable.pause);
        }
    }
}