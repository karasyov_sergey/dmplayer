package ru.pbt.dmplayer.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import ru.pbt.dmplayer.R;
import ru.pbt.dmplayer.adapters.TrackListAdapter;
import ru.pbt.dmplayer.audio.AudioFile;

/**
 * Класс, представляющий собой активити,
 * отображающий список треклистов относительно
 * выбранного альбома или исполнителя
 *
 * @author Анастасия Лисова, 17ИТ17
 */
public class TrackListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.general_fragment);

        MainActivity activity = MainActivity.getInstance();
        ArrayList<AudioFile> tracks = activity.getAudioList();
        ArrayList<AudioFile> targetTracks = new ArrayList<>();
        Intent intent = getIntent();

        if (intent.hasExtra("album")) {
            for (int i = 0; i < tracks.size(); i++) {
                if (tracks.get(i).getAlbum().equals(intent.getStringExtra("album"))) {
                    targetTracks.add(tracks.get(i));
                }
            }
            sortTrackByAlbum(targetTracks);
        } else if (intent.hasExtra("artist")) {
            for (int i = 0; i < tracks.size(); i++) {
                if (tracks.get(i).getArtist().equals(intent.getStringExtra("artist"))) {
                    targetTracks.add(tracks.get(i));
                }
            }
            activity.sortTrack(targetTracks);
        }

        TrackListAdapter adapter = new TrackListAdapter(targetTracks);
        LinearLayoutManager manager = new LinearLayoutManager(getApplicationContext());

        RecyclerView recyclerView = findViewById(R.id.rec_view);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
    }

    /**
     * Метод для сортировки списка треков
     * по позиции его в альбоме
     *
     * @param tracks список треков
     */
    private void sortTrackByAlbum(ArrayList<AudioFile> tracks) {
        for (int j = tracks.size() - 1; j >= 0; j--) {
            for (int i = 0; i < j; i++) {
                if (tracks.get(i).getAlbumNum() > tracks.get(i + 1).getAlbumNum()) {
                    AudioFile file = tracks.get(i);
                    tracks.set(i, tracks.get(i + 1));
                    tracks.set(i + 1, file);
                }
            }
        }
    }
}
