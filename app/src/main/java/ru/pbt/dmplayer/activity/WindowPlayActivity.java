package ru.pbt.dmplayer.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;

import ru.pbt.dmplayer.R;
import ru.pbt.dmplayer.audio.AudioFile;

/**
 * Класс для представления окна
 * воспроизведения музыкальных файлов
 *
 * @author Лисова Анастасия, 17ИТ17
 */
public class WindowPlayActivity extends AppCompatActivity {
    private MediaPlayer mediaPlayer;
    private int currentPlayed;
    private ArrayList<AudioFile> currentAudioList;
    private TextView textView;
    private SeekBar time;
    int totalTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.window_play_activity);

        MainActivity activity = MainActivity.getInstance();
        mediaPlayer = activity.getMediaPlayer();

        currentPlayed = activity.getCurrentPlayed();
        currentAudioList = activity.getCurrentAudioList();

        textView = findViewById(R.id.audio_name);
        setCurrentPlayedName();

        time = findViewById(R.id.time);
        totalTime = mediaPlayer.getDuration();
        time.setMax(totalTime);
        setOnSeekBar();
    }

    /**
     * Метод для перемотки песни
     */
    private void setOnSeekBar() {
        time.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if(fromUser){
                            mediaPlayer.seekTo(progress);
                            time.setProgress(progress);
                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                }
        );
    }

    /**
     * Метод для выведения информации об
     * играющей песне
     */
    private void setCurrentPlayedName() {
        AudioFile file = currentAudioList.get(currentPlayed);
        String artist = file.getArtist();
        String name = file.getAudioName();
        String textViewData = artist + " - " + name;
        textView.setText(textViewData);
    }

    /**
     * Метод для приостановки и обратного
     * воспроизведения музыкального файла
     */
    public void onClickBtnPlay(View view) {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
            ((ImageButton) view).setImageResource(R.drawable.play);

        } else if (!mediaPlayer.isPlaying()) {
            mediaPlayer.start();
            ((ImageButton) view).setImageResource(R.drawable.pause);
        }
    }

    /**
     * Метод для переключения
     * на следующий музыкальный файл
     */
    public void onClickBtnNext(View view) throws IOException {
        mediaPlayer.stop();
        mediaPlayer.reset();

        if (currentPlayed == currentAudioList.size() - 1) {
            currentPlayed = 0;
        } else {
            currentPlayed += 1;
        }
        mediaPlayer.setDataSource(currentAudioList.get(currentPlayed).getPath());
        mediaPlayer.prepare();
        mediaPlayer.start();
        setCurrentPlayedName();
    }

    /**
     * Метод для переключения
     * на предыдущий музыкальный файл
     */
    public void onClickBtnPrev(View view) throws IOException {
        mediaPlayer.stop();
        mediaPlayer.reset();

        if (currentPlayed == 0) {
            currentPlayed = currentAudioList.size() - 1;
        } else {
            currentPlayed -= 1;
        }
        mediaPlayer.setDataSource(currentAudioList.get(currentPlayed).getPath());
        mediaPlayer.prepare();
        mediaPlayer.start();
        setCurrentPlayedName();
    }
}
