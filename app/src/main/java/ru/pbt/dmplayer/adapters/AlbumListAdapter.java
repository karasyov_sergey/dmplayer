package ru.pbt.dmplayer.adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import ru.pbt.dmplayer.R;
import ru.pbt.dmplayer.activity.TrackListActivity;

/**
 * Класс, представляющий собой адаптер
 * для списка альбомов
 *
 * @author KSO 17ИТ17
 */
public class AlbumListAdapter extends RecyclerView.Adapter<AlbumListAdapter.AlbumHolder> {
    private ArrayList<String> albumList;

    public AlbumListAdapter(ArrayList<String> albumList) {
        this.albumList = albumList;
    }

    @NonNull
    @Override
    public AlbumHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.track_cell, parent, false);
        return new AlbumHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumHolder holder, int position) {
        String albumName = albumList.get(position);
        holder.album.setText(albumName);
    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }

    /**
     * Класс, представляющий собой холдер
     * для каждого элемента адаптера
     *
     * @author Анастасия Лисова, 17ИТ17
     */
    class AlbumHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView album;

        AlbumHolder(@NonNull View itemView) {
            super(itemView);
            album = itemView.findViewById(R.id.track);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), TrackListActivity.class);
            intent.putExtra("album", album.getText().toString());
            v.getContext().startActivity(intent);
        }
    }
}
