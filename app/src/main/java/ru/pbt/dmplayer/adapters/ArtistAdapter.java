package ru.pbt.dmplayer.adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import ru.pbt.dmplayer.R;
import ru.pbt.dmplayer.activity.TrackListActivity;

/**
 * Класс, представляющий собой адаптер
 * для списка исполнителей
 *
 * @author KSO 17ИТ17
 */
public class ArtistAdapter extends RecyclerView.Adapter<ArtistAdapter.ArtistHolder> {

    private ArrayList<String> artists;

    public ArtistAdapter(ArrayList<String> artists) {
        this.artists = artists;
    }

    @NonNull
    @Override
    public ArtistHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.track_cell, parent, false);
        return new ArtistHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ArtistHolder holder, int position) {
        holder.artist.setText(artists.get(position));
    }

    @Override
    public int getItemCount() {
        return artists.size();
    }

    /**
     * Класс, представляющий собой холдер
     * для каждого элемента адаптера
     *
     * @author Анастасия Лисова, 17ИТ17
     */
    class ArtistHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView artist;

        ArtistHolder(@NonNull View itemView) {
            super(itemView);
            artist = itemView.findViewById(R.id.track);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), TrackListActivity.class);
            intent.putExtra("artist", artist.getText().toString());
            v.getContext().startActivity(intent);
        }
    }
}
