package ru.pbt.dmplayer.adapters;

import android.media.MediaPlayer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.util.ArrayList;

import ru.pbt.dmplayer.activity.MainActivity;
import ru.pbt.dmplayer.R;
import ru.pbt.dmplayer.audio.AudioFile;

/**
 * Класс, представляющий собой адаптер
 * для списка треков
 *
 * @author KSO 17ИТ17
 */
public class TrackListAdapter extends RecyclerView.Adapter<TrackListAdapter.TrackHolder> {
    private ArrayList<AudioFile> trackList;
    private MediaPlayer player;
    private MainActivity activity;

    public TrackListAdapter(ArrayList<AudioFile> trackList) {
        this.trackList = trackList;
        activity = MainActivity.getInstance();
        player = activity.getMediaPlayer();
        activity.setCurrentAudioList(trackList);
    }


    @NonNull
    @Override
    public TrackHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.track_cell, parent, false);
        return new TrackHolder(view);
    }

    @Override
    public int getItemCount() {
        return trackList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull TrackHolder holder, int position) {
        AudioFile audioFile = trackList.get(position);
        String artist = audioFile.getArtist();
        String title = audioFile.getAudioName();
        String holderData = artist + " - " + title;
        holder.track.setText(holderData);
        holder.media = audioFile;
        holder.id = position;
    }

    /**
     * Класс, представляющий собой холдер
     * для каждого элемента адаптера
     *
     * @author Анастасия Лисова, 17ИТ17
     */
    class TrackHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView track;
        private AudioFile media;
        private int id;

        TrackHolder(@NonNull View itemView) {
            super(itemView);
            track = itemView.findViewById(R.id.track);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            activity.setCurrentPlayed(id);
            if (player.isPlaying()) {
                player.stop();
                player.reset();
            }

            try {
                player.setDataSource(media.getPath());
                player.prepare();
                player.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}