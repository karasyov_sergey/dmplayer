package ru.pbt.dmplayer.audio;

import android.media.MediaMetadataRetriever;

/**
 * Класс для формирование основной
 * информации о ауио - файле
 * (путь к файлу, название трека,
 * исполнитель, альбом, порядковый
 * номер в альбоме)
 *
 * @author Лисова Анастасия, 17ИТ17
 */
public class AudioFile {
    private String path;
    private String artist;
    private String audioName;
    private String album;
    private int albumNum;

    public AudioFile(String path) {
        this.path = path;
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        mmr.setDataSource(path);
        String data = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
        artist = (data != null) ? data : "unknown";
        data = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
        audioName = (data != null) ? data : "unknown";
        data = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
        album = (data != null) ? data : "unknown";
        data = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_CD_TRACK_NUMBER);
        try {
            albumNum = Integer.parseInt(data);
        } catch (NumberFormatException e) {
            albumNum = 0;
        }
    }

    public String getPath() {
        return path;
    }

    public String getArtist() {
        return artist;
    }

    public String getAudioName() {
        return audioName;
    }

    public String getAlbum() {
        return album;
    }

    public int getAlbumNum() {
        return albumNum;
    }
}
