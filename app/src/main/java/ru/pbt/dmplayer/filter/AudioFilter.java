package ru.pbt.dmplayer.filter;

import java.io.File;
import java.io.FileFilter;

/**
 * Класс, играющий роль фильтра файлов
 * по критерию: адуиофайл или нет?
 *
 * @author KSO 17ИТ17
 */
public class AudioFilter implements FileFilter {
    private static final String[] extensions = {".mp3", ".m4a",".wav"};

    @Override
    public boolean accept(File pathname) {
        String fileName = pathname.getName();
        if (fileName.length() < 4) return false;
        for (String ext : extensions) {
            if (fileName.endsWith(ext)) return true;
        }
        return false;
    }


}
