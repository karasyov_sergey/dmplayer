package ru.pbt.dmplayer.ui.albums;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import ru.pbt.dmplayer.activity.MainActivity;
import ru.pbt.dmplayer.R;
import ru.pbt.dmplayer.adapters.AlbumListAdapter;
/**
 * Класс, представляющий собой фрагмент,
 * отображающий список альбомов текущего треклиста
 *
 * @author KSO 17ИТ17
 */
public class AlbumListFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.general_fragment, container, false);

        MainActivity activity = MainActivity.getInstance();

        AlbumListAdapter adapter = new AlbumListAdapter(activity.getAlbumList());
        LinearLayoutManager manager = new LinearLayoutManager(root.getContext());

        RecyclerView recyclerView = root.findViewById(R.id.rec_view);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
        return root;
    }
}